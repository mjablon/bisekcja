﻿// ConsoleApplication8.cpp : Defines the entry point for the console application.
//


/* Program napisała Magdalena Jabłońska*/


#include "stdafx.h"
#include<iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>


using namespace std;
const double EPS0 = 0.0001; // dokładność porównania z zerem
const double EPSX = 0.0001; // dokładność wyznaczenia pierwiastka

double horner(int n, double x); // funkcja wyliczająca wartość wielomianu algorytmem Hornera
double polowienie_przedzialow(double c, double d, double epsilon); // funkcja wyliczająca pierwiastki wielomianu metodą bisekcji
int j, n, t, s;

double c, d;
float x, a[20];
int main()
{
	cout << "OBLICZANIE PIERWIASTKOW FUNKCJI METODA BISEKCJI" << endl << endl;
	cout << "Podaj stopien wielomianu: ";
	cin >> n;
	cout << "Podaj X: ";
	cin >> x;
	cout << "Wprowadz wspolczynniki wielomianu:" << endl;

	for (int i = 0; i <= n; i++)
		cin >> a[i];

	cout << "Wartosc wielomianu = " << horner(n, x) << endl; // wartość wielomianu dla daner wartości

	cout << "Podaj poczatek przedzialu: ";
	cin >> c;
	cout << "Podaj koniec przedziału: ";
	cin >> d;
	cout << "Podaj liczbe iteracji: ";
	cin >> t;

	cout << " wynik : " << polowienie_przedzialow(c, d, EPS0); // otrzymane miejsce zerowe

	system("pause");
}


double horner(int stopien, double x)
{
	if (stopien == 0)
		return a[0];

	else if (stopien > 19) // przekroczony zakres

		return false;

	else

		return horner(stopien - 1, x)*x + a[stopien]; // obliczenie warosci wielomianu w n mnożeniach
}

double polowienie_przedzialow(double c, double d, double epsilon)
{

	for (j = 0; j < t; j++)
	{
		s = (c + d) / 2;
		if ((horner(n, c)*horner(n, s) < 0) && (horner(n, s)*horner(n, d) > 0)) // sprawdzenie wartosci funkcji oraz warunku  
		{
			d = s; // zamiana wartosci d do wczesnij obliczonego srodka przedzialu
		}
		else if ((horner(n, c)*horner(n, s) > 0) && (horner(n, s)*horner(n, d) < 0)) 
		{
			c = s;
		}
		else if (abs(horner(n, s)) < epsilon) // sprawdzenie warunku epsilon >c−d
		{
			cout << "Znaleziono wynik = " << s << " po " << j << " iteracjach" << endl << endl;
			break;
		}
	}
	return s;
}
